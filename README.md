The goal here is to build a thing that looks at my Google Play Music account and recommend shows to see based on music I like (maybe based on "Thumbs Up" playlist and maybe even write a algorithm that ranks what I should see).

# Installation

Coming soon but ultimately something like `npm i`.

# APIs

Google Music API:

- https://github.com/simon-weber/gmusicapi
- https://www.npmjs.com/package/playmusic

Live Music API:

- http://www.songkick.com/developer
- http://api.eventful.com/

Other APIs:

- http://www.programmableweb.com/news/160-music-apis-last.fm-soundcloud-and-spotify/2012/01/18
